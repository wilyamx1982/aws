# AWS CLI


### Switching regions

**[get config settings](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)**
	
	(tt-app-server) $ aws configure get region --profile tit
	us-west-1   
	(tt-app-server) $ aws configure get region --profile dtp
	ap-southeast-1
	
	(tt-app-server) $ aws configure set region dtp
	(tt-app-server) $ aws configure get region    
	dtp
